# Gulp Skeleton

Repositorio con la estructura y documentación para empezar un proyecto con GULP. 

### Requerimientos

Se requieren los siquientes paquetes:

* [Node.js](https://nodejs.org/) para el manejo de dependencias.
* [Gulp](https://gulpjs.com/) para manejo de las tareas.

### Instalar ambiente de desarrollo

Luego de clonar el proyecto, instalar las dependencias con el comando:

```bash
npm install
```

### Tareas disponibles

Tareas principales

```bash
gulp help                         # Muestra ayuda sobre los comandos disponibles
gulp build                        # Genera los archivos para el ambiente de desarrollo
gulp build --env=production       # Genera los archivos para el ambiente de producción
gulp server                       # Inicia el servidor web y detecta los cambios
gulp test                         # Ejecuta los Linters y las pruebas unitarias
```

Tareas parciales

```bash
gulp assets                       # Exporta todos los archivos estáticos
gulp styles                       # Genera todos los archivos CSS
gulp styles --file=filename.scss  # Genera el archivo CSS definido como parámetro
gulp scripts                      # Genera todos los archivos Javascript
gulp scripts --file=filename.js   # Genera el archivo Javascript definido como parámetro
gulp views                        # Genera todos los archivos HTML
gulp views --file=filename.pug    # Genera el archivo HTML definido como parámetro
```

### Estructura de Archivos 

El proyecto tiene la siguiente estructura:

* `src/` Carpeta para código fuente del proyecto.
* `dist/` Archivos listos para producción.
* `public/` Archivos temporales de desarrollo.
* `test/` Archivos para pruebas unitarias.
* `doc/` Archivos con la documentación del proyecto.

La carpeta con el código fuente tiene la siguiente estructura: 

* `scripts/` Archivos fuente en Javascript.
* `styles/` Archivos fuente en SASS.
* `views/` Archivos fuente en PUG.
* `assets/` Archivos estáticos (imágenes, fuentes, etc).

Adicionalmente se tienen los archivos de configuración

* `.editorconfig` Configuración del editor de código.
* `.browserslistrc` Configuración de Navegadores soportados (Autoprefixer, Babel).
* `.sass-lint.yml` Configuración con las reglas de SassLint.
* `.eslintrc.json` Configuración con las reglas de ESLint.
* `.eslintignore` Configuración ignorar archivos de ESLint.
* `.gitignore` Configuración para ignorar archivos en GIT.
* `gulpfile.js` Archivo con la definición de tareas en GULP.
