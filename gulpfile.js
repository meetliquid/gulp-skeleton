/* globals process, exports, require */
/* eslint-disable no-console */
/* exported cb */

// declare dependencies
const { watch, series, parallel, src, dest } = require('gulp'),
  pkg = require('./package.json'),
  pug = require('gulp-pug'),
  sass = require('gulp-sass'),
  babel = require('gulp-babel'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  sourcemaps= require('gulp-sourcemaps'),
  browserSync = require('browser-sync').create(),
  autoprefixer = require('gulp-autoprefixer'),
  sasslint = require('gulp-sass-lint'),
  eslint = require('gulp-eslint'),
  del = require('del');

// declare local params
let file = '',
  env = (process.env.NODE_ENV || 'development'),
  colors = { yellow: '\x1b[33m', green: '\x1b[32m', reset: '\x1b[0m', red: '\x1b[31m' },
  paths = {
    'assets': 'src/assets/',
    'styles': 'src/styles/',
    'scripts': 'src/scripts/',
    'vendor': 'src/scripts/vendor/',
    'views': 'src/views/',
  },
  source = {
    'assets': paths.assets + '**/*.*',
    'styles': paths.styles + '*.scss',
    'scripts': [paths.scripts + '**/*.*', '!' + paths.vendor + '*.*'],
    'vendor': paths.vendor + '*.js',
    'views': paths.views + '*.pug',
  };

function init (cb) {
  // parse enviroment
  let params = process.argv.slice(2);

  params.forEach( (param) => {
    if (param.startsWith('--env=')) {
      param = param.split('=');
      env = param[1];
    } else {
      if (param.startsWith('--file=')) {
        param = param.split('=');
        file = param[1];
      }
    }
  });

  cb();
}

function help (cb) {
  if (pkg.help) {
    pkg.help.forEach( (section) => {
      console.group(colors.yellow, '\n' + section.title + ':');
      section.tasks.forEach( (task) => {
        console.log(colors.green, (task.name).padEnd(40), colors.reset, task.value);
      });
      console.groupEnd();
    });
    console.log('');
  } else {
    console.log(colors.yellow, '\nHelp is not defined!', colors.reset, '\n');
  }

  cb();
}

function clean () {
  if (env === 'production') {
    return del('dist/');
  } else {
    return del('public/');
  }
}

function styles () {
  let files = source.styles;

  if (file != '') {
    files = paths.styles + file;
  }

  if (env === 'production') {
    return src(files)
      .pipe(sass({outputStyle: 'compressed'}))
      .pipe(autoprefixer())
      .pipe(dest('dist/styles'));
  } else {
    return src(files)
      .pipe(sourcemaps.init())
      .pipe(sass({outputStyle: 'expanded'}))
      .pipe(sourcemaps.write())
      .pipe(autoprefixer())
      .pipe(dest('public/styles'))
      .pipe(browserSync.stream());
  }
}

function scripts () {
  let files = source.scripts;

  if (file != '') {
    files = paths.scripts + file;
  }

  if (env === 'production') {
    return src(files)
      .pipe(babel())
      .pipe(uglify())
      .pipe(dest('dist/scripts'));
  } else {
    return src(files)
      .pipe(sourcemaps.init())
      .pipe(babel())
      .pipe(sourcemaps.write())
      .pipe(dest('public/scripts'));
  }
}

function vendors (cb) {
  let files = source.vendor;

  if (file != '') {
    cb();
    return;
  }

  if (env === 'production') {
    return src(files)
      .pipe(concat('vendors.js'))
      .pipe(uglify({keep_fnames: true}))
      .pipe(dest('dist/scripts'));
  } else {
    return src(files)
      .pipe(concat('vendors.js'))
      .pipe(dest('public/scripts'));
  }
}

function views () {
  let files = source.views;

  if (file != '') {
    files = paths.views + file;
  }

  if (env === 'production') {
    return src(files)
      .pipe(pug({'pretty' : false}))
      .pipe(dest('dist/'));
  } else {
    return src(files)
      .pipe(pug({'pretty' : true}))
      .pipe(dest('public/'));
  }
}

function assets () {
  let files = source.assets;

  if (env === 'production') {
    return src(files)
      .pipe(dest('dist'));
  } else {
    return src(files)
      .pipe(dest('public'));
  }
}

function listen () {
  watch(source.assets, series(assets));
  watch(source.scripts, series(scripts, reload));
  watch(source.vendor, series(vendors));
  watch(source.views, series(views, reload));
  watch(source.styles, series(styles, stream));
}

function reload (cb) {
  browserSync.reload();
  cb();
}

function stream (cb) {
  browserSync.stream();
  cb();
}

function server (cb) {
  browserSync.init({
    notify: false,
    server: {
      baseDir: './public',
      directory: true
    }
  });
  cb();
}

function stylesTest () {
  console.log(colors.yellow, '\nStarting SassLint', colors.reset, '\n');

  return src(paths.styles + '**/*.*')
    .pipe(sasslint())
    .pipe(sasslint.format())
    .pipe(sasslint.failOnError());
}

function scriptsTest () {
  console.log(colors.yellow, '\nStarting ESLint', colors.reset, '\n');
  return src(source.scripts)
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
}

// export tasks
exports.help = help;
exports.assets = series(init, assets);
exports.styles = series(init, styles);
exports.scripts = series(init, scripts, vendors);
exports.views = series(init, views);
exports.test = series(stylesTest, scriptsTest);
exports.build = series(init, clean, stylesTest, scriptsTest, styles, scripts, vendors, views, assets);
exports.server = parallel(server, listen);
